<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
Use App\Http\Resources\PostCollection;
Use App\Http\Resources\Post as PostResource;
use App\Post;

class PostsController extends Controller
{
    public function all()
    {
        return new PostCollection(Post::with('user')->get());
    }

    public function show(Post $post)
    {
        return new PostResource(Post::find($post->id));
    }
}
