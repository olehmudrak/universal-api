<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
Use App\Http\Resources\UserCollection;
use App\User;

class UsersController extends Controller
{
    public function all()
    {
        return User::with('posts')->get();
    }

    public function show(User $user)
    {
        return new UserCollection(User::with('posts')->find($user));
    }
}
