<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Post;
use Faker\Generator as Faker;
use Illuminate\Support\Arr;

$factory->define(Post::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence(6),
        'content' => Arr::random([$faker->paragraph(3), null]),
        'image' => Arr::random([
            'https://images.pexels.com/photos/373076/pexels-photo-373076.jpeg?cs=srgb&dl=blank-business-composition-computer-373076.jpg&fm=jpg',
            'https://images.pexels.com/photos/2148217/pexels-photo-2148217.jpeg?cs=srgb&dl=turned-on-black-laptop-computer-on-table-2148217.jpg&fm=jpg'.
            null, null]),
        'user_id' => factory(App\User::class)->create()->id
    ];
});
