<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $posts = factory(App\User::class, 5)->create()->each(function ($user) {
            factory(App\Post::class, 5)->create(['user_id' => $user->id]);
        });
    }
}
